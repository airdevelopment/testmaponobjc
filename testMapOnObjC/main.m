//
//  main.m
//  testMapOnObjC
//
//  Created by Eugeniya Pervushina on 11/4/17.
//  Copyright © 2017 air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
