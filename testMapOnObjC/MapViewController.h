//
//  MapViewController.h
//  testMapOnObjC
//
//  Created by Eugeniya Pervushina on 11/4/17.
//  Copyright © 2017 air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController <GMSMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, retain) IBOutlet GMSMapView * googleMaps;

@end
