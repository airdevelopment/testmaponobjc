//
//  AppDelegate.h
//  testMapOnObjC
//
//  Created by Eugeniya Pervushina on 11/4/17.
//  Copyright © 2017 air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

