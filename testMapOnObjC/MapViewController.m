//
//  MapViewController.m
//  testMapOnObjC
//
//  Created by Eugeniya Pervushina on 11/4/17.
//  Copyright © 2017 air. All rights reserved.
//

#import "MapViewController.h"
#import <AFNetworking.h>

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface MapViewController ()

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *locationStart;
@property (strong, nonatomic) CLLocation *locationEnd;
@property (strong, nonatomic) NSArray *pointsList;

@end

@implementation MapViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
    
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDelegate:self];
    [_locationManager setDistanceFilter:kCLHeadingFilterNone];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [_locationManager setPausesLocationUpdatesAutomatically:NO];
    [_locationManager startUpdatingLocation];
    
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    _locationStart = [[CLLocation alloc] initWithLatitude:50.448880 longitude:30.522805];
    _locationEnd = [[CLLocation alloc] initWithLatitude:52.517756 longitude:13.377355];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_locationStart.coordinate.latitude
                                                            longitude:_locationStart.coordinate.longitude
                                                                 zoom:13];
    
    self.googleMaps.delegate = self;
    self.googleMaps.camera = camera;
    
    self.googleMaps.myLocationEnabled = YES;
    self.googleMaps.settings.myLocationButton = YES;
    self.googleMaps.settings.compassButton = YES;
    self.googleMaps.settings.zoomGestures = YES;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"kiev_berlin_locals" ofType:@"plist"];
    _pointsList = [NSArray arrayWithContentsOfFile:path];
    
    [self.view layoutIfNeeded];
}

// MARK: function for create a marker pin on map
- (void) createMarker:(NSString*) titleMarker latitude:(CLLocationDegrees)latitude longitude: (CLLocationDegrees)longitude {
    GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitude, longitude)];
    marker.title = titleMarker;
    marker.map = _googleMaps;
}

- (void) drawStartEndPointsLine {
    [self createMarker:@"pointA" latitude: _locationStart.coordinate.latitude longitude: _locationStart.coordinate.longitude];
    [self createMarker:@"pointB" latitude: _locationEnd.coordinate.latitude longitude: _locationEnd.coordinate.longitude];
    
    [self drawPath:_locationStart endLocation: _locationEnd];
}

//MARK: - Location Manager delegates

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_locationStart.coordinate.latitude
                                                            longitude:_locationStart.coordinate.longitude
                                                                 zoom:8];
    
    [self drawStartEndPointsLine];
    
    [self.googleMaps animateToCameraPosition:camera];
    [self.locationManager stopUpdatingLocation];
    
    CLLocation* point = [[CLLocation alloc] initWithLatitude:_locationStart.coordinate.latitude longitude:_locationStart.coordinate.longitude];
    
    [self fetchPointsByRadius:point];
    [self drawStartEndPointsLine];
    
}

// MARK: - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    _googleMaps.myLocationEnabled = YES;
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    _googleMaps.myLocationEnabled = YES;
    
    if (gesture) {
        mapView.selectedMarker = nil;
    }
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {

    _googleMaps.myLocationEnabled = YES;
    
    [self.googleMaps clear];
    
    return false;
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
//    print("COORDINATE \(coordinate)")
}

- (BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView {
    _googleMaps.myLocationEnabled = YES;
    _googleMaps.selectedMarker = nil;
    return false;
}

//MARK: create direction path

- (void) drawPath:(CLLocation*) startLocation endLocation:(CLLocation*) endLocation {
    NSString* urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=%@", startLocation.coordinate.latitude, startLocation.coordinate.longitude, endLocation.coordinate.latitude, endLocation.coordinate.longitude, @"AIzaSyAvV2OJ2h8DOF2csRjjzb_DhIiezzM1mF0"];//, &language=%@ @"en"];
    
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:urlStr]];
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
          if (!error) {
              NSError *jsonError;
              NSDictionary *dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options:nil error:&jsonError];
              if (!jsonError) {
                  
                  NSArray *routesArray = (NSArray*)dict[@"routes"];
                  NSMutableArray *points = [NSMutableArray array];
                  
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      for (NSDictionary *route in routesArray) {
                          NSDictionary *overviewPolyline = route[@"overview_polyline"];
                          [points addObject:overviewPolyline[@"points"]];
                          
                          NSString *points = [overviewPolyline objectForKey:@"points"];
                          
                          GMSPath *polyLinePath = [GMSPath pathFromEncodedPath:points];
                          GMSPolyline *polyline = nil;
                          polyline = [GMSPolyline polylineWithPath:polyLinePath];
                          polyline.strokeColor = [UIColor redColor];
                          polyline.strokeWidth = 3.f;
                          polyline.map = _googleMaps;
                          
                          GMSMutablePath *path = [GMSMutablePath path];
                          for (int p = 0; p < polyLinePath.count; p++) {
                              [path addCoordinate:[polyLinePath coordinateAtIndex:p]];
                          }
                          NSLog(@"path %@", path);
                          
                          NSArray* array = [self polylineWithEncodedString:points];
                          [self fetchPointsByPoints:array];
                      }
                  });
              }
          } else {
              //print error message
              NSLog(@"%@", [error localizedDescription]);
          }
      }] resume];
}

//MARK: fetchPointsByRadius

- (void) fetchPointsByRadius:(CLLocation*) coordinate {
    
    for (NSString *string in _pointsList) {
        NSArray* coordArr = [string componentsSeparatedByString:@", "];
        CLLocation* somePoint = [[CLLocation alloc] initWithLatitude:[coordArr[0] doubleValue] longitude:[coordArr[1] doubleValue]];
        CLLocationDistance distance = [coordinate distanceFromLocation:somePoint];

        if (distance <= 1000) {
            [self createMarker:@"point" latitude: somePoint.coordinate.latitude longitude: somePoint.coordinate.longitude];
        }
    }
}

- (void) fetchPointsByPoints: (NSArray *)points {

    for (CLLocation *point in points) {
        for (NSString *string in _pointsList) {
            NSArray* coordArr = [string componentsSeparatedByString:@", "];
            CLLocation* somePoint = [[CLLocation alloc] initWithLatitude:[coordArr[0] doubleValue] longitude:[coordArr[1] doubleValue]];
            CLLocationDistance distance = [point distanceFromLocation:somePoint];
            
            if (distance <= 1000) {
                [self createMarker:@"point" latitude: somePoint.coordinate.latitude longitude: somePoint.coordinate.longitude];
            }
        }
    }
    
}

- (NSArray *)polylineWithEncodedString:(NSString *)encodedString {
    const char *bytes = [encodedString UTF8String];
    NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    NSUInteger count = length / 4;
    CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
    NSUInteger coordIdx = 0;
    
    float latitude = 0;
    float longitude = 0;
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    while (idx < length) {
        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        coords[coordIdx++] = coord;
        
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
        }
        CLLocation *location = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
        [array addObject:location];
    }
    
    return [NSArray arrayWithArray:array];
}


@end
